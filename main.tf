provider "aws" {
  region = "eu-west-1"
}
variable "vpc_cidr_block" {}
variable "subnet_cidr_block" {}
variable "avail_zone" {}
variable "env_prefix" {}
variable "my_ip" {}
variable "instance_type" {}
variable "public_key_loc" {}

resource "aws_vpc" "myapp-vpc" {
  cidr_block = var.vpc_cidr_block
  tags = {
    "Name" = "${var.env_prefix}-vpc"
  }
}

resource "aws_subnet" "myapp-subnet-1" {
  vpc_id = aws_vpc.myapp-vpc.id
  cidr_block = var.subnet_cidr_block
  availability_zone = var.avail_zone
  tags = {
    "Name" = "${var.env_prefix}-subnet-1"
  } 
}

resource "aws_route_table" "myapp-route-table" {
  vpc_id = aws_vpc.myapp-vpc.id
  route  {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.myapp-igw.id
  } 
  tags = {
    "Name" = "${var.env_prefix}-rtb"
  }
}

resource "aws_internet_gateway" "myapp-igw" {
  vpc_id = aws_vpc.myapp-vpc.id

    tags = {
    "Name" = "${var.env_prefix}-igw"
  }
}

resource "aws_route_table_association" "a-rtb-subnet" {
  subnet_id = aws_subnet.myapp-subnet-1.id
  route_table_id = aws_route_table.myapp-route-table.id
}

resource "aws_security_group" "myapp-sg" {
  name = "myapp-sg"
  vpc_id = aws_vpc.myapp-vpc.id
  
  ingress = [ {
    cidr_blocks = [ var.my_ip ]
    description = "my house - allow ssh in"
    from_port = 22
    protocol = "tcp"
    self = false
    to_port = 22
    security_groups = null
    prefix_list_ids = null
    ipv6_cidr_blocks = null
  } ,
  {
    cidr_blocks = [ "0.0.0.0/0" ]
    description = "everywhere - allow 8080 in"
    from_port = 8080
    ipv6_cidr_blocks = null
    prefix_list_ids = null
    protocol = "tcp"
    security_groups = null
    self = false
    to_port = 8080
  } ]
  egress = [ {
    cidr_blocks = [ "0.0.0.0/0" ]
    description = "everywhere - allow wibblies out"
    from_port = 0
    ipv6_cidr_blocks = null
    prefix_list_ids = []
    protocol = "-1"
    security_groups = null
    self = false
    to_port = 0
  } ]

  tags = {
    "Name" = "${var.env_prefix}-sg"
  }
}

data "aws_ami" "latest-amazon-linux-image" {
  most_recent = true
  owners = [ "amazon" ]
  filter {
    name = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2" ]
  }
  filter {
    name = "virtualization-type"
    values = ["hvm"]
  }
}

output "aws_ami_id" {
  value = data.aws_ami.latest-amazon-linux-image.id
}

/* automated key-pair from id_rsa.pub local file
  Doesn't seem to work with my local file and i cba */
/*
resource "aws_key_pair" "ssh-key" {
  key_name = "serv-key"
  public_key = "file(var.public_key_loc)"
}
*/

resource "aws_instance" "myapp-server" {
  ami = data.aws_ami.latest-amazon-linux-image.id
  instance_type = var.instance_type
  
  subnet_id = aws_subnet.myapp-subnet-1.id
  vpc_security_group_ids = [ aws_security_group.myapp-sg.id ]
  availability_zone = var.avail_zone

  associate_public_ip_address = true
  key_name = "server-kp-simon-01686"
  /*key_name = aws_key_pair.ssh-key.key_name*/

/*
  user_data = <<EOF
                  #!/bin/bash
                  sudo yum update -y && sudo yum install docker -y
                  sudo systemctl start docker
                  sudo usermod -aG  docker ec2-user
                  docker run -p 8080:80 nginx
              EOF
*/
  user_data = file("entry-script.sh")

  tags = {
    "name" = "${var.env_prefix}-server"
  }

}

/*
output "ec2_public_ip" {
  value = data.aws_instance.myapp-server.public_ip
}
*/

